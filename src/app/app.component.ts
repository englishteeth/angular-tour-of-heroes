import { ConfigurationService } from './configuration.service';
import { Component } from '@angular/core';
import { OAuthService, NullValidationHandler, AuthConfig } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';

  authConfig: AuthConfig;

  constructor(private oauthService: OAuthService, config: ConfigurationService) {
    this.authConfig = {
      issuer: config.AUTH_ISSUER,
      redirectUri: window.location.origin + '/heroes',
      clientId: config.AUTH_CLIENT_ID,
      scope: config.AUTH_SCOPE,
      responseType: config.AUTH_RESPONSE_TYPE,
      // at_hash is not present in JWT token
      disableAtHashCheck: config.AUTH_DISABLE_AT_HASH_CHECK,
      showDebugInformation: config.AUTH_SHOW_DEBUG_INFORMATION
    };

    this.configure();
  }

  public login() {
    this.oauthService.initLoginFlow();
  }

  public logoff() {
    this.oauthService.logOut();
  }

  private configure() {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

}
