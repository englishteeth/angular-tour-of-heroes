import { APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { Observable, ObservableInput, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { ConfigurationService } from './configuration.service';

import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';

function load(http: HttpClient, config: ConfigurationService): (() => Promise<boolean>) {
  return (): Promise<boolean> => {
    return new Promise<boolean>((resolve: (a: boolean) => void): void => {
      http.get('./config/application.json')
      .pipe(
        map((x: ConfigurationService) => {
          console.log('LOADING CONFIG FILE');
          config.AUTH_CLIENT_ID = x.AUTH_CLIENT_ID;
          config.AUTH_DISABLE_AT_HASH_CHECK = x.AUTH_DISABLE_AT_HASH_CHECK;
          config.AUTH_ISSUER = x.AUTH_ISSUER;
          config.AUTH_RESPONSE_TYPE = x.AUTH_RESPONSE_TYPE;
          config.AUTH_SCOPE = x.AUTH_SCOPE;
          config.AUTH_SHOW_DEBUG_INFORMATION = x.AUTH_SHOW_DEBUG_INFORMATION;
          resolve(true);
        }),
        catchError((x: { status: number }, caught: Observable<void>): ObservableInput<{}> => {
          if (x.status !== 404) {
            resolve(false);
          }
          config.AUTH_ISSUER = 'auth provider url';
          config.AUTH_CLIENT_ID = 'auth provider client id';
          config.AUTH_RESPONSE_TYPE = 'code';
          config.AUTH_SCOPE = 'openid profile email offline_access heroes';
          config.AUTH_DISABLE_AT_HASH_CHECK = true;
          config.AUTH_SHOW_DEBUG_INFORMATION = true;
          resolve(true);
          return of({});
        })
      ).subscribe();
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    DashboardComponent,
    HeroSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
          allowedUrls: ['http://localhost:8080/api'],
          sendAccessToken: true
      }
    }),
    AppRoutingModule
  ],
  providers: [{
    provide: APP_INITIALIZER,
    deps: [
      HttpClient,
      ConfigurationService
    ],
    useFactory: load,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
