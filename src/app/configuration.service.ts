import { Injectable } from '@angular/core';
import { Observable, ObservableInput, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  AUTH_ISSUER: string;
  AUTH_CLIENT_ID: string;
  AUTH_SCOPE: string;
  AUTH_RESPONSE_TYPE: string;
  AUTH_DISABLE_AT_HASH_CHECK: boolean;
  AUTH_SHOW_DEBUG_INFORMATION: boolean;

  constructor() { }

}
